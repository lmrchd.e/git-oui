<?php


namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class testController
{
    const TEST_URL = 'f:/Works/git-oui';

    /**
     * @Route("/test", name="test")
     */
    public function index() {

        $repo = new \Gitonomy\Git\Repository(self::TEST_URL);
        var_dump($repo->getReferences()->getBranch('master'));
        die();
    }
}